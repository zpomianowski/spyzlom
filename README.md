# Spyzlom
Backend for *Donosiciel* (Android app for *Złombol* community).
Created by Zbigniew Pomianowski <zbigniew.pomianowski@gmail.com>
Tested with Ubuntu 14.04.


### Dependencies
Make sure virtualenv is set and packages from `requirements.txt` (if exists) are properly installed (via pip).
Some need additional libraries (install via apt).


### Production environment
+ nginx
+ uwsgi
+ postgres/mysql/sqlite db
+ upstart/systemd
