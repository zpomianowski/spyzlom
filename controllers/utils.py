# -*- coding: utf-8 -*-

# from gluon.contrib.login_methods.oauth20_account import OAuthAccount


# # import required classes
# import httplib2
# from apiclient.discovery import build
# from oauth2client.client import flow_from_clientsecrets
# from oauth2client.file import Storage
# from oauth2client.tools import run

# # Declare constants and set configuration values

# # The file with the OAuth 2.0 Client details for authentication and authorization.
# CLIENT_SECRETS = request.folder + 'static\\google_client_secrets.json'
# print CLIENT_SECRETS

# # A helpful message to display if the CLIENT_SECRETS file is missing.
# MISSING_CLIENT_SECRETS_MESSAGE = '%s is missing' % CLIENT_SECRETS

# # The Flow object to be used if we need to authenticate.
# FLOW = flow_from_clientsecrets(
#     CLIENT_SECRETS,
#     scope='https://www.googleapis.com/auth/analytics.readonly',
#     message='MISSING CLIENT SECRETS!')

# # A file to store the access token
# TOKEN_FILE_NAME = 'analytics.dat'


# def prepare_credentials():
#     # Retrieve existing credendials
#     storage = Storage(TOKEN_FILE_NAME)
#     credentials = storage.get()

#     # If existing credentials are invalid and Run Auth flow
#     # the run method will store any new credentials
#     if credentials is None or credentials.invalid:
#         #run Auth Flow and store credentials
#         credentials = run(FLOW, storage)

#     return credentials


# def initialize_service():
#     # 1. Create an http object
#     http = httplib2.Http()

#     # 2. Authorize the http object
#     # In this tutorial we first try to retrieve stored credentials. If
#     # none are found then run the Auth Flow. This is handled by the
#     # prepare_credentials() function defined earlier in the tutorial
#     credentials = prepare_credentials()
#     http = credentials.authorize(http)  # authorize the http object

#     # 3. Build the Analytics Service Object with the authorized http object
#     return build('analytics', 'v3', http=http)

import json
import os
from oauth2client.client import SignedJwtAssertionCredentials

# The scope for the OAuth2 request.
SCOPE = 'https://www.googleapis.com/auth/analytics.readonly'

# The location of the key file with the key data.
KEY_FILEPATH = 'gdzieonikurdesa-8761f43d40f4.json'

# Load the key file's private data.
with open(os.path.join(request.folder, 'private', KEY_FILEPATH)) as key_file:
    _key_data = json.load(key_file)

# Construct a credentials objects from the key data and OAuth2 scope.
_credentials = SignedJwtAssertionCredentials(
    _key_data['client_email'], _key_data['private_key'], SCOPE)


def stats():
    return dict(token=_credentials.get_access_token().access_token)


def fb_parse():
    response.view = 'generic.json'
    return parse_signed_request(request.args[0])
