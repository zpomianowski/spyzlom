# -*- coding: utf-8 -*-
import helpers

response.title = 'Gdzie oni kurde są!'
response.subtitle = T('O! Chyba tutaj:)')

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Zbigniew Pomianowski'
response.meta.description = 'Śledzenie pozycji ekip Złombola. Dla ekip Złombola, przyjaciół, rodziny i bliskich ;)'
response.meta.keywords = 'złombol, autofocus, śledzenie floty, śledzenie GPS, web2py, python, framework'
response.meta.generator = 'Web2py Web Framework'
response.application = 'Gdzie oni kurde są!'

## your http://google.com/analytics id
response.google_analytics_id = 'UA-53992275-1'

auth.navbar(mode='bare')
welcome = T('User settings') if auth.bar['user'] is None \
    else auth.bar['prefix'] + auth.bar['user']
response.menu = [
    [welcome, False, None, []]
]
for k, v in auth.bar.items():
    if k == 'prefix' or k == 'user':
        continue
    response.menu[0][3] += [(k, False, v, [])]

response.menu += [
    (T('Map'), False, URL('default', 'index'), []),
    (T('Android App'), False, URL('default', 'androidapp'), []),
    (T('Share'), False, URL('default', 'help'), []),
    (T("Site stats"), False, URL('utils', 'stats'), []),
    (T('Contact'), False, "mailto:zbigniew.pomianowski@gmail.com", []),
    (T('Thanks to'), False, URL('default', 'thanks'), []),
    (IMG(_src=URL('static', 'images/logo-32black.png'), _id="autofocus"),
        False,
        "https://www.facebook.com/autofocusZlombol2013?fref=ts", []),
    (IMG(_src=URL('static', 'images/logo_zlombol-32.png'), _id="zlombol"),
        False,
        "http://zlombol.pl/", []),
    (IMG(_src=URL('static', 'images/magurski.png'), _id="magurski"),
        False,
        "http://gdzieonikurdesa.pl/magurski", [])
]
