# -*- coding: utf-8 -*-
import hmac
import gluon.contrib.simplejson as json
from base64 import urlsafe_b64decode
from hashlib import sha256
from gluon.contrib.appconfig import AppConfig

myconf = AppConfig(reload=True)

MAGURSKI = False
FB_SECRET = "<secret_key_to_fill>"

custom_icons = {
    1:
    {"color": "special", "vehicle_type": "autofocus"}
}


def parse_signed_request(signed_request):
    [encoded_sig, payload] = signed_request.split('.')

    # decode data
    sig = base64_url_decode(encoded_sig)
    data = json.loads(base64_url_decode(payload))

    if data['algorithm'].upper() != 'HMAC-SHA256':
        raise ValueError('Unknown algorithm. Expected HMAC-SHA256')

    # check sig
    expected_sig = hmac.new(FB_SECRET, payload, sha256).digest()
    if sig != expected_sig:
        raise StandardError('Bad Signed JSON signature!')

    return data


def base64_url_decode(input):
    input += '=' * (4 - (len(input) % 4))
    return urlsafe_b64decode(input.encode('utf-8'))
