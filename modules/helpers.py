# -*- coding: utf-8 -*-

from gluon import *


def gen_menu(div, menu_list, elid=None):
    ul = UL(_id=elid)
    div.append(ul)
    for item in menu_list:
        idx = item[0]
        menu_item = None
        if idx == 'login':
            k = current.T('Login')
        elif idx == 'register':
            k = current.T('Register')
        elif idx == 'request_reset_password':
            k = current.T('Lost password?')
        elif idx == 'retrieve_username':
            k = current.T('Forgot username?')
        elif idx == 'logout':
            k = current.T('Logout')
        elif idx == 'profile':
            k = current.T('Profile')
        elif idx == 'change_password':
            k = current.T('Password')
        elif idx == 'teams':
            k = current.T('Teams')
        elif idx == 'extra':
            menu_item = LI(item[1])
        else:
            k = idx

        if len(item) > 2:
            href = '#' if item[2] is None else item[2]
            menu_item = LI(A(k, _href=href))
            if len(item) > 3:
                if len(item[3]) > 0:
                    menu_item = LI(A(k, _href=href, _class='ic ic-left'))
                    div2 = DIV(_class='mp-level')
                    div2.append(A(current.T('back'), _class='mp-back'))
                    menu_item.append(gen_menu(div2, item[3]))
        ul.append(menu_item)
    return div


def MENU(menu):
    div = DIV(_class='mp-level')
    return gen_menu(div, menu)
