# -*- coding: utf-8 -*-
{
'!langcode!': 'ru',
'!langname!': 'Русский',
'"update" is an optional expression like "field1=\'newvalue\'". You cannot update or delete the results of a JOIN': '"Изменить" - необязательное выражение вида "field1=\'новое значение\'". Результаты операции JOIN нельзя изменить или удалить.',
'%d days ago': '%d %%{день} тому',
'%d hours ago': '%d %%{час} тому',
'%d minutes ago': '%d %%{минуту} тому',
'%d months ago': '%d %%{месяц} тому',
'%d seconds ago': '%d %%{секунду} тому',
'%d weeks ago': '%d %%{неделю} тому',
'%d years ago': '%d %%{год} тому',
'%s %%{row} deleted': '%%{!удалена[0]} %s %%{строка[0]}',
'%s %%{row} updated': '%%{!изменена[0]} %s %%{строка[0]}',
'%s days ago': '%s days ago',
'%s selected': '%%{!выбрана[0]} %s %%{запись[0]}',
'%Y-%m-%d': '%Y-%m-%d',
'%Y-%m-%d %H:%M:%S': '%Y-%m-%d %H:%M:%S',
'1 day ago': '1 день тому',
'1 hour ago': '1 час тому',
'1 minute ago': '1 минуту тому',
'1 month ago': '1 месяц тому',
'1 second ago': '1 секунду тому',
'1 week ago': '1 неделю тому',
'1 year ago': '1 год тому',
'4x4': '4x4',
'About': 'About',
'Access Control': 'Access Control',
"Add app to the FB's fanpage": "Add app to the FB's fanpage",
'Add to your fanpage': 'Add to your fanpage',
'Administrative Interface': 'Administrative Interface',
'Administrative interface': 'административный интерфейс',
'Ajax Recipes': 'Ajax Recipes',
'An error occured, please %s the page': 'An error occured, please %s the page',
'Android App': 'Android App',
'appadmin is disabled because insecure channel': 'appadmin is disabled because insecure channel',
'Aqua': 'Aqua',
'Are you sure you want to delete this object?': 'Вы уверены, что хотите удалить этот объект?',
'Available Databases and Tables': 'Базы данных и таблицы',
'back': 'back',
'Black': 'Black',
'Blue': 'Blue',
'Blueviolet': 'Blueviolet',
'Brown': 'Brown',
'Bulldozer': 'Bulldozer',
'Burly wood': 'Burly wood',
'Bus': 'Bus',
'Buy this book': 'Buy this book',
'cache': 'cache',
'Cache': 'Cache',
'Cache Keys': 'Cache Keys',
'Cadetblue': 'Cadetblue',
'Cannot be empty': 'Пустое значение недопустимо',
'Change Password': 'Смените пароль',
'Check if you are looking for help or would like to inform about a failure.': 'Check if you are looking for help or would like to inform about a failure.',
'Check to delete': 'Удалить',
'Check to delete:': 'Удалить:',
'Chocolate': 'Chocolate',
'City': 'City',
'City of origin': 'City of origin',
'Clear CACHE?': 'Clear CACHE?',
'Clear DISK': 'Clear DISK',
'Clear RAM': 'Clear RAM',
'Client IP': 'Client IP',
'Close': 'Close',
'Community': 'Community',
'Components and Plugins': 'Components and Plugins',
'Confirm Password': 'Confirm Password',
'Contact': 'Contact',
'Contact email': 'Contact email',
'Contact phone': 'Contact phone',
'Controller': 'Controller',
'Convertible': 'Convertible',
'Copyright': 'Copyright',
'Coral': 'Coral',
'Created By': 'Created By',
'Created On': 'Created On',
'Crew': 'Crew',
'Crew members': 'Crew members',
'Crew size': 'Crew size',
'Crimson': 'Crimson',
'Current request': 'Текущий запрос',
'Current response': 'Текущий ответ',
'Current session': 'Текущая сессия',
'customize me!': 'настройте внешний вид!',
'Darkgreen': 'Darkgreen',
'data uploaded': 'данные загружены',
'Database': 'Database',
'Database %s select': 'выбор базы данных %s',
'db': 'БД',
'DB Model': 'DB Model',
'Delete:': 'Удалить:',
'Demo': 'Demo',
'Deployment Recipes': 'Deployment Recipes',
'Describe what is the problem. Maybe someone can help you.': 'Describe what is the problem. Maybe someone can help you.',
'Description': 'Описание',
'design': 'дизайн',
'Detailed team info': 'Detailed team info',
'Direct link to %s team': 'Direct link to %s team',
'DISK': 'DISK',
'Disk Cache Keys': 'Disk Cache Keys',
'Disk Cleared': 'Disk Cleared',
'Documentation': 'Documentation',
"Don't know what to do?": "Don't know what to do?",
'done!': 'готово!',
'Download': 'Download',
'Download *.kmz': 'Download *.kmz',
'Download samples': 'Download samples',
'E-mail': 'E-mail',
'Edit current record': 'Редактировать текущую запись',
'Edit Profile': 'Редактировать профиль',
'Email and SMS': 'Email and SMS',
'enter a number between %(min)g and %(max)g': 'enter a number between %(min)g and %(max)g',
'Enter a number between %(min)g and %(max)g': 'Enter a number between %(min)g and %(max)g',
'Enter an integer between %(min)g and %(max)g': 'Enter an integer between %(min)g and %(max)g',
'enter an integer between %(min)g and %(max)g': 'enter an integer between %(min)g and %(max)g',
'Enter an integer greater than or equal to %(min)g': 'Enter an integer greater than or equal to %(min)g',
'Errors': 'Errors',
'Example': 'Example',
'export as csv file': 'экспорт в  csv-файл',
'Fanpage': 'Fanpage',
'FAQ': 'FAQ',
'First name': 'Имя',
'For incomplete data file generated for the last month.': 'For incomplete data file generated for the last month.',
'Forms and Validators': 'Forms and Validators',
'Free Applications': 'Free Applications',
'From day': 'From day',
'From you to others': 'From you to others',
'Fuchsia': 'Fuchsia',
'Funpage': 'Funpage',
'Gold': 'Gold',
'Gray': 'Gray',
'Group ID': 'Group ID',
'Groups': 'Groups',
'Hello World': 'Заработало!',
'Home': 'Home',
'How did you get here?': 'How did you get here?',
'Icon Colour': 'Icon Colour',
'Icon type': 'Icon type',
'import': 'import',
'Import/Export': 'Импорт/экспорт',
'Info!': 'Info!',
'insert new': 'добавить',
'insert new %s': 'добавить %s',
'Internal State': 'Внутренне состояние',
'Introduction': 'Introduction',
'Invalid email': 'Неверный email',
'Invalid login': 'Неверный логин',
'Invalid password': 'Неверный пароль',
'Invalid Query': 'Неверный запрос',
'invalid request': 'неверный запрос',
'Is Active': 'Is Active',
'Jeep': 'Jeep',
'Key': 'Key',
'Khaki': 'Khaki',
'Last name': 'Фамилия',
'Last update': 'Last update',
'Layout': 'Layout',
'Layout Plugins': 'Layout Plugins',
'Layouts': 'Layouts',
'Lime': 'Lime',
'Live Chat': 'Live Chat',
'Loading...': 'Loading...',
'Log In': 'Log In',
'Logged in': 'Вход выполнен',
'Logged out': 'Выход выполнен',
'login': 'вход',
'Login': 'Вход',
'logout': 'выход',
'Logout': 'Выход',
'Lost Password': 'Забыли пароль?',
'Lost password?': 'Lost password?',
'Manage Cache': 'Manage Cache',
'Map': 'Map',
'Maps': 'Maps',
'Menu Model': 'Menu Model',
'Modified By': 'Modified By',
'Modified On': 'Modified On',
'Motor': 'Motor',
'My Sites': 'My Sites',
'Name': 'Name',
'New password': 'Новый пароль',
'New Record': 'Новая запись',
'new record inserted': 'новая запись добавлена',
'next 100 rows': 'следующие 100 строк',
'No databases in this application': 'В приложении нет баз данных',
'now': 'сейчас',
'O! Chyba tutaj:)': 'O! Chyba tutaj:)',
'Object or table name': 'Object or table name',
'Old password': 'Старый пароль',
'Online examples': 'примеры он-лайн',
'open in new tab': 'open in new tab',
'or import from csv file': 'или импорт из csv-файла',
'Origin': 'Происхождение',
'Other Plugins': 'Other Plugins',
'Other Recipes': 'Other Recipes',
'Overview': 'Overview',
'Password': 'Пароль',
'password': 'пароль',
"Password fields don't match": 'Пароли не совпадают',
'Picture': 'Picture',
'Pink': 'Pink',
'Place iframe on your page': 'Place iframe on your page',
'Please do not register if not a member of Złombol team': 'Please do not register if not a member of Złombol team',
'please input your password again': 'please input your password again',
'Plugins': 'Plugins',
'Powered by': 'Powered by',
'Preface': 'Preface',
'previous 100 rows': 'предыдущие 100 строк',
'Problem description': 'Problem description',
'profile': 'профиль',
'Python': 'Python',
'Query:': 'Запрос:',
'Quick Examples': 'Quick Examples',
'RAM': 'RAM',
'RAM Cache Keys': 'RAM Cache Keys',
'Ram Cleared': 'Ram Cleared',
'Recipes': 'Recipes',
'Record': 'Record',
'record does not exist': 'запись не найдена',
'Record ID': 'ID записи',
'Record id': 'id записи',
'Red': 'Red',
'Register': 'Зарегистрироваться',
'Registration identifier': 'Registration identifier',
'Registration key': 'Ключ регистрации',
'reload': 'reload',
'Remember me (for 30 days)': 'Запомнить меня (на 30 дней)',
'Request reset password': 'Request reset password',
'Reset Password key': 'Сбросить ключ пароля',
'Role': 'Роль',
'Rows in Table': 'Строк в таблице',
'Rows selected': 'Выделено строк',
'Samples Nb: ': 'Samples Nb: ',
'Scooter': 'Scooter',
'Sea': 'Sea',
'Search…': 'Search…',
'Semantic': 'Semantic',
'Services': 'Services',
'Share': 'Share',
'Show/Hide': 'Show/Hide',
'Sign Up': 'Sign Up',
'Site stats': 'Site stats',
'Size of cache:': 'Size of cache:',
'Speed': 'Speed',
'Sport car': 'Sport car',
'state': 'состояние',
'Statistics': 'Statistics',
'Stylesheet': 'Stylesheet',
'submit': 'submit',
'Submit': 'Отправить',
'Super motor': 'Super motor',
'Support': 'Support',
'Sure you want to delete this object?': 'Подтвердите удаление объекта',
'SUV': 'SUV',
'Table': 'таблица',
'Table name': 'Имя таблицы',
'Taxi': 'Taxi',
'Team': 'Team',
'Team ID': 'Team ID',
'TEAM_ID': 'TEAM_ID',
'Teams': 'Teams',
'The "query" is a condition like "db.table1.field1==\'value\'". Something like "db.table1.field1==db.table2.field2" results in a SQL JOIN.': '"Запрос" - это условие вида "db.table1.field1==\'значение\'". Выражение вида "db.table1.field1==db.table2.field2" формирует SQL JOIN.',
'The Core': 'The Core',
'The output of the file is a dictionary that was rendered by the view %s': 'The output of the file is a dictionary that was rendered by the view %s',
'The Views': 'The Views',
'This App': 'This App',
'This email already has an account': 'This email already has an account',
'Time in Cache (h:m:s)': 'Time in Cache (h:m:s)',
'Timestamp': 'Отметка времени',
'To day': 'To day',
'Truck': 'Truck',
'Twitter': 'Twitter',
'unable to parse csv file': 'нечитаемый csv-файл',
'Update:': 'Изменить:',
'Use (...)&(...) for AND, (...)|(...) for OR, and ~(...)  for NOT to build more complex queries.': 'Для построение сложных запросов используйте операторы "И": (...)&(...), "ИЛИ": (...)|(...), "НЕ": ~(...).',
'User %(id)s Logged-in': 'Пользователь %(id)s вошёл',
'User %(id)s Logged-out': 'Пользователь %(id)s вышел',
'User %(id)s Password changed': 'Пользователь %(id)s сменил пароль',
'User %(id)s Profile updated': 'Пользователь %(id)s обновил профиль',
'User %(id)s Registered': 'Пользователь %(id)s зарегистрировался',
'User ID': 'ID пользователя',
'User settings': 'User settings',
'Van': 'Van',
'Vehicle': 'Vehicle',
'Verify Password': 'Повторите пароль',
'Videos': 'Videos',
'View': 'View',
'Visible only to logged in Złombol members': 'Visible only to logged in Złombol members',
'Visible to the public': 'Visible to the public',
'Warning!': 'Warning!',
'We have a failure': 'We have a failure',
'Webpage': 'Webpage',
'Website': 'Website',
'Welcome': 'Welcome',
'Welcome to web2py': 'Добро пожаловать в web2py',
'Welcome to web2py!': 'Welcome to web2py!',
'Which called the function %s located in the file %s': 'Which called the function %s located in the file %s',
'Yellowgreen': 'Yellowgreen',
'You are successfully running web2py': 'You are successfully running web2py',
'You can modify this application and adapt it to your needs': 'You can modify this application and adapt it to your needs',
'You visited the url %s': 'You visited the url %s',
'Вход': 'Вход',
'Зарегистрироваться': 'Зарегистрироваться',
}
