# -*- coding: utf-8 -*-
{
'!langcode!': 'tr',
'!langname!': 'Türkçe',
'%s %%(shop)': '%s %%(shop)',
'%s %%(shop[0])': '%s %%(shop[0])',
'%s %%{quark[0]}': '%s %%{quark[0]}',
'%s %%{shop[0]}': '%s %%{shop[0]}',
'%s %%{shop}': '%s %%{shop}',
'%s days ago': '%s days ago',
'%Y-%m-%d': '%Y-%m-%d',
'%Y-%m-%d %H:%M:%S': '%Y-%m-%d %H:%M:%S',
'@markmin\x01**Hello World**': '**Merhaba Dünya**',
'About': 'Hakkında',
'Access Control': 'Erişim Denetimi',
'Administrative Interface': 'Yönetim Arayüzü',
'Ajax Recipes': 'Ajax Tarifleri',
'An error occured, please %s the page': 'Bir hata meydana geldi, lütfen sayfayı %s',
'Android App': 'Android App',
'Are you sure you want to delete this object?': 'Bu nesneyi silmek istediğinden emin misin?',
'back': 'back',
'Buy this book': 'Bu kitabı satın alın',
'Cannot be empty': 'Boş bırakılamaz',
'Check to delete': 'Silmek için denetle',
'City of origin': 'City of origin',
'Client IP': ' IP',
'Close': 'Close',
'Community': 'Topluluk',
'Components and Plugins': 'Bileşenler ve Eklentiler',
'Contact': 'Contact',
'Controller': 'Denetçi',
'Copyright': 'Telif',
'Created By': 'Tasarlayan',
'Created On': 'Oluşturma tarihi',
'Crew': 'Crew',
'Crew members': 'Crew members',
'customize me!': 'burayı değiştir!',
'Database': 'Veritabanı',
'DB Model': 'DB Model',
'Demo': 'Demo',
'Deployment Recipes': 'Yayınlama tarifleri',
'Description': 'Açıklama',
'Detailed team info': 'Detailed team info',
'Documentation': 'Kitap',
"Don't know what to do?": 'Neleri nasıl yapacağını bilmiyor musun?',
'Download': 'İndir',
'Download *.kmz': 'Download *.kmz',
'Download samples': 'Download samples',
'E-mail': 'E-posta',
'Email and SMS': 'E-posta ve kısa mesaj (SMS)',
'Enter a number between %(min)g and %(max)g': 'Enter a number between %(min)g and %(max)g',
'Enter an integer between %(min)g and %(max)g': 'Enter an integer between %(min)g and %(max)g',
'enter an integer between %(min)g and %(max)g': '%(min)g ve %(max)g arasında bir sayı girin',
'Enter an integer greater than or equal to %(min)g': 'Enter an integer greater than or equal to %(min)g',
'enter date and time as %(format)s': 'tarih ve saati %(format)s biçiminde girin',
'Errors': 'Hatalar',
'Fanpage': 'Fanpage',
'FAQ': 'SSS',
'First name': 'Ad',
'For incomplete data file generated for the last month.': 'For incomplete data file generated for the last month.',
'Forgot username?': 'Kullanıcı adını mı unuttun?',
'Forms and Validators': 'Biçimler ve Doğrulayıcılar',
'Free Applications': 'Ücretsiz uygulamalar',
'From day': 'From day',
'Giriş': 'Giriş',
'Group %(group_id)s created': '%(group_id)s takımı oluşturuldu',
'Group ID': 'Takım ID',
'Group uniquely assigned to user %(id)s': 'Group uniquely assigned to user %(id)s',
'Groups': 'Topluluklar',
'Hello World': 'Merhaba Dünyalı',
'Hello World  ## comment': 'Merhaba Dünyalı  ',
'Hello World## comment': 'Merhaba Dünyalı',
'Home': 'Anasayfa',
'How did you get here?': 'Bu sayfayı görüntüleme uğruna neler mi oldu?',
'Info!': 'Info!',
'Introduction': 'Giriş',
'Invalid email': 'Yanlış eposta',
'Is Active': 'Etkin',
'Kayıt ol': 'Kayıt ol',
'Last name': 'Soyad',
'Last update': 'Last update',
'Layout': 'Şablon',
'Layout Plugins': 'Şablon Eklentileri',
'Layouts': 'Şablonlar',
'Live Chat': 'Canlı Sohbet',
'Loading...': 'Loading...',
'Log In': 'Log In',
'Logged in': 'Giriş yapıldı',
'Logged out': 'Çıkış yapıldı',
'Login': 'Giriş',
'Logout': 'Terket',
'Lost Password': 'Şifremi unuttum',
'Lost password?': 'Şifrenizimi unuttunuz?',
'Map': 'Map',
'Maps': 'Maps',
'Menu Model': 'Menu Model',
'Modified By': 'Değiştiren',
'Modified On': 'Değiştirilme tarihi',
'My Sites': 'Sitelerim',
'Name': 'İsim',
'Object or table name': 'Nesne ya da tablo adı',
'Online examples': 'Canlı örnekler',
'open in new tab': 'open in new tab',
'Origin': 'Origin',
'Other Plugins': 'Diğer eklentiler',
'Other Recipes': 'Diğer Tarifler',
'Overview': 'Göz gezdir',
'Password': 'Şifre',
"Password fields don't match": 'Şifreler uyuşmuyor',
'please input your password again': 'lütfen şifrenizi tekrar girin',
'Plugins': 'Eklentiler',
'Powered by': 'Yazılım Temeli',
'Preface': 'Preface',
'Problem description': 'Problem description',
'Profile': 'Hakkımda',
'Python': 'Python',
'Quick Examples': 'Hızlı Örnekler',
'Recipes': 'Recipes',
'Record ID': 'Kayıt ID',
'Register': 'Kayıt ol',
'Registration identifier': 'Registration identifier',
'Registration key': 'Kayıt anahtarı',
'Registration successful': 'Kayıt başarılı',
'reload': 'reload',
'Remember me (for 30 days)': 'Beni hatırla (30 gün)',
'Request reset password': 'Şifre sıfırla',
'Reset Password key': 'Şifre anahtarını sıfırla',
'Role': 'Role',
'Samples Nb: ': 'Samples Nb: ',
'Search…': 'Search…',
'Semantic': 'Semantik',
'Services': 'Hizmetler',
'Share': 'Share',
'Show/Hide': 'Show/Hide',
'Sign Up': 'Sign Up',
'Site stats': 'Site stats',
'Speed': 'Speed',
'Stylesheet': 'Stil Şablonu',
'Support': 'Destek',
'Team ID': 'Team ID',
'Teams': 'Teams',
'The Core': 'Çekirdek',
'The output of the file is a dictionary that was rendered by the view %s': 'Son olarak fonksiyonların vs. işlenip %s dosyasıyla tasarıma yedirilmesiyle sayfayı görüntüledin',
'The Views': 'The Views',
'This App': 'Bu Uygulama',
'This email already has an account': 'This email already has an account',
'Timestamp': 'Zaman damgası',
'To day': 'To day',
'Twitter': 'Twitter',
'User %(id)s Logged-in': '%(id)s Giriş yaptı',
'User %(id)s Logged-out': '%(id)s çıkış yaptı',
'User %(id)s Password reset': 'User %(id)s Password reset',
'User %(id)s Registered': '%(id)s Kayıt oldu',
'User ID': 'Kullanıcı ID',
'User settings': 'User settings',
'value already in database or empty': 'değer boş ya da veritabanında zaten mevcut',
'Vehicle': 'Vehicle',
'Verify Password': 'Şifreni Onayla',
'Videos': 'Videolar',
'View': 'View',
'Webpage': 'Webpage',
'Welcome': 'Hoşgeldin',
'Welcome to web2py!': 'Welcome to web2py!',
'Which called the function %s located in the file %s': 'Bu ziyaretle %s fonksiyonunu %s dosyasından çağırmış oldun ',
'You are successfully running web2py': 'web2py çatısını çalıştırmayı başardın',
'You can modify this application and adapt it to your needs': 'Artık uygulamayı kafana göre düzenleyebilirsin!',
'You visited the url %s': '%s adresini ziyaret ettin',
}
